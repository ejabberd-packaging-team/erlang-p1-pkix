erlang-p1-pkix (1.0.10-2) unstable; urgency=medium

  * Updated Standards-Version: 4.7.0 (no changes needed)
  * Updated years in debian/copyright
  * debian/rules: add creation of symbolic link for native app name

 -- Philipp Huebner <debalance@debian.org>  Sun, 09 Feb 2025 10:57:05 +0100

erlang-p1-pkix (1.0.10-1) unstable; urgency=medium

  * New upstream version 1.0.10

 -- Philipp Huebner <debalance@debian.org>  Thu, 03 Oct 2024 17:41:44 +0200

erlang-p1-pkix (1.0.9-3) unstable; urgency=medium

  * Dropped debian/erlang-p1-pkix.install
  * Updated lintian overrides
  * Renamed debian/erlang-p1-pkix.lintian-overrides
    -> debian/lintian-overrides

 -- Philipp Huebner <debalance@debian.org>  Tue, 26 Dec 2023 16:11:45 +0100

erlang-p1-pkix (1.0.9-2) unstable; urgency=medium

  [ Debian Janitor ]
  * Update renamed lintian tag names in lintian overrides.
  * Set field Upstream-Contact in debian/copyright.
  * Remove obsolete fields Contact, Name from debian/upstream/metadata
    (already present in machine-readable debian/copyright).

  [ Philipp Huebner ]
  * Updated Standards-Version: 4.6.2 (no changes needed)
  * Updated years in debian/copyright

 -- Philipp Huebner <debalance@debian.org>  Wed, 08 Feb 2023 12:55:34 +0100

erlang-p1-pkix (1.0.9-1) unstable; urgency=medium

  * New upstream version 1.0.9
  * Updated Standards-Version: 4.6.1 (no changes needed)
  * Updated Erlang dependencies
  * Updated years in debian/copyright

 -- Philipp Huebner <debalance@debian.org>  Thu, 02 Jun 2022 10:28:07 +0200

erlang-p1-pkix (1.0.8-2) unstable; urgency=medium

  [ Jenkins ]
  * Remove constraints unnecessary since buster

  [ Philipp Huebner ]
  * Updated debian/watch

 -- Philipp Huebner <debalance@debian.org>  Sat, 18 Dec 2021 13:56:06 +0100

erlang-p1-pkix (1.0.8-1) unstable; urgency=medium

  * New upstream version 1.0.8
  * Updated Standards-Version: 4.6.0 (no changes needed)

 -- Philipp Huebner <debalance@debian.org>  Sun, 29 Aug 2021 21:32:00 +0200

erlang-p1-pkix (1.0.7-3) unstable; urgency=medium

  * Corrected Multi-Arch setting to "allowed"

 -- Philipp Huebner <debalance@debian.org>  Sun, 31 Jan 2021 18:43:29 +0100

erlang-p1-pkix (1.0.7-2) unstable; urgency=medium

  * Added 'Multi-Arch: same' in debian/control
  * Updated years in debian/copyright

 -- Philipp Huebner <debalance@debian.org>  Sat, 30 Jan 2021 17:55:58 +0100

erlang-p1-pkix (1.0.7-1) unstable; urgency=medium

  * New upstream version 1.0.7
  * Updated Standards-Version: 4.5.1 (no changes needed)
  * Updated version of debian/watch: 4
  * Updated debhelper compat version: 13
  * Updated lintian overrides
  * Added debian/erlang-p1-pkix.install

 -- Philipp Huebner <debalance@debian.org>  Fri, 25 Dec 2020 23:41:07 +0100

erlang-p1-pkix (1.0.6-1) unstable; urgency=medium

  * New upstream version 1.0.6

 -- Philipp Huebner <debalance@debian.org>  Sun, 02 Aug 2020 17:10:53 +0200

erlang-p1-pkix (1.0.5-1) unstable; urgency=medium

  * New upstream version 1.0.5

 -- Philipp Huebner <debalance@debian.org>  Tue, 17 Mar 2020 17:48:40 +0100

erlang-p1-pkix (1.0.4-2) unstable; urgency=medium

  * Updated Standards-Version: 4.5.0 (no changes needed)
  * Updated years in debian/copyright
  * Rules-Requires-Root: no

 -- Philipp Huebner <debalance@debian.org>  Wed, 05 Feb 2020 17:37:26 +0100

erlang-p1-pkix (1.0.4-1) unstable; urgency=medium

  * New upstream version 1.0.4
  * Updated Standards-Version: 4.4.1 (no changes needed)
  * Set 'Rules-Requires-Root: no' in debian/control

 -- Philipp Huebner <debalance@debian.org>  Mon, 11 Nov 2019 16:10:52 +0100

erlang-p1-pkix (1.0.3-1) unstable; urgency=medium

  * New upstream version 1.0.3

 -- Philipp Huebner <debalance@debian.org>  Sat, 10 Aug 2019 14:03:09 +0200

erlang-p1-pkix (1.0.2-1) unstable; urgency=medium

  * New upstream version 1.0.2
  * Updated Erlang dependencies
  * Added debian/upstream/metadata
  * Updated Standards-Version: 4.4.0 (no changes needed)
  * debian/rules: export ERL_COMPILER_OPTIONS=deterministic
  * Updated debhelper compat version: 12

 -- Philipp Huebner <debalance@debian.org>  Wed, 24 Jul 2019 12:17:53 +0200

erlang-p1-pkix (1.0.0-3) unstable; urgency=medium

  * Updated debian/copyright
  * Fixed incorrect $DESTDIR in debian/rules
  * Added debian/docs to install README.md

 -- Philipp Huebner <debalance@debian.org>  Mon, 31 Dec 2018 01:56:27 +0100

erlang-p1-pkix (1.0.0-2) unstable; urgency=medium

  * Added ca-certificates to (Build-)Depends (Closes: #917589)

 -- Philipp Huebner <debalance@debian.org>  Sat, 29 Dec 2018 11:59:23 +0100

erlang-p1-pkix (1.0.0-1) unstable; urgency=medium

  * New upstream version 1.0.0
  * Updated Standards-Version: 4.3.0 (no changes needed)

 -- Philipp Huebner <debalance@debian.org>  Sat, 29 Dec 2018 01:59:56 +0100

erlang-p1-pkix (0.2018.11.12-1) unstable; urgency=medium

  * Initial release (Closes: #915288)

 -- Philipp Huebner <debalance@debian.org>  Sun, 02 Dec 2018 16:31:36 +0100
